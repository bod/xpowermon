#ifndef _misc_h
#define _misc_h

/*
 *  Miscellaneous macros.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#ifdef __GNUC__
# define ATTRIB_UNUSED		__attribute__ ((unused))
# define ATTRIB_NORETURN	__attribute__ ((noreturn))
# define ATTRIB_PRINTF(fmt,var)	__attribute__ ((format(printf,fmt,var)))
#else /* __GNUC__ */
# define ATTRIB_UNUSED		/* nothing */
# define ATTRIB_NORETURN	/* nothing */
# define ATTRIB_PRINTF(fmt,var)	/* nothing */
#endif /* __GNUC__ */

#endif /* _misc_h */
