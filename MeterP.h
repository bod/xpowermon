#ifndef _MeterP_h
#define _MeterP_h

/*
 *  Meter widget private data.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include "Meter.h"
#include <X11/Xaw/SimpleP.h>

/* New fields for the Meter widget class record */
typedef struct {
    XtPointer extension;
} MeterClassPart;

/* Class record declaration */
typedef struct _MeterClassRec {
    CoreClassPart core_class;
    SimpleClassPart simple_class;
    MeterClassPart meter_class;
} MeterClassRec;

extern MeterClassRec meterClassRec;

/* New fields for the Meter widget record */
typedef struct {
    /* resources */
    Pixel foreground;		/* scale colour */
    Pixel needle;		/* needle colour */
    int min;			/* leftmost value */
    int max;			/* rightmost value */
    int value;			/* current value */
    Dimension internal_width;	/* padding on either side of scale */
    Dimension internal_height;	/* padding above and below scale */
    Boolean show_value;		/* display current value: */
    XFontStruct *font;		/* ...using font */
    Pixel font_color;		/* ...in colour */
    String units;		/* ...and optional units */
    int scale_factor;		/* ...scaled by this factor */
    int precision;		/* ...decimal places */

    /* private state */
    Pixmap _scale;		/* pixmap for background (ticks/label) */
    Dimension _scale_width;	/* scale pixmap size */
    Dimension _scale_height;
    Dimension _scale_r;		/* radius */
    GC _scale_gc;		/* GC for scale */
    GC _scale_fill_gc;		/* GC for scale */
    GC _gc;			/* GC for window */
    GC _label_gc;		/* GC for label */
} MeterPart;

/* Instance record declaration */
typedef struct _MeterRec {
    CorePart core;
    SimplePart simple;
    MeterPart meter;
} MeterRec;

#endif /* _MeterP_h */
