/*
 *  /sys/class/power_supply parsing for xpowermon.
 *
 *  Copyright (c) 2002, 2003, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <dirent.h>
#include <math.h>
#include <err.h>
#include "power.h"

/* Scan power directory for BATn subdirectories */
int battery_count()
{
    DIR *power_supply = opendir(POWER_SUPPLY_DIR);
    int count = 0;
    struct dirent *ent;
    if (!power_supply)
	err(1, "can't open %s", POWER_SUPPLY_DIR);

    while ((ent = readdir(power_supply)))
    {
	if (!strncmp(ent->d_name, "BAT", 3) && isdigit(ent->d_name[3]))
	{
	    /* Expect BAT0, BAT1, so keep track of the largest seen.
	       Missing batteries are ignored be read_battery_info(). */
	    int bat = atoi(ent->d_name + 3);
	    if ((bat + 1) > count)
		count = bat + 1;
	}
    }

    closedir(power_supply);
    return count;
}

/* Read stuff from power_supply files */
static char const *_read_str(char const *file)
{
    static char buf[BUFSIZ];
    char *str = NULL;
    FILE *fp = fopen(file, "r");
    if (!fp)
    {
	warn("can't open %s", file);
	return NULL;
    }

    if (fgets(buf, sizeof(buf), fp))
    {
	char *p = strchr(str = buf, '\n');
	if (p)
	    *p = '\0';
    }

    fclose(fp);
    return str;
}

static int _read_int(char const *file)
{
    char const *contents = _read_str(file);
    if (!contents)
	return -1;

    while (isspace(*contents))
	contents++;

    if (isdigit(*contents))
	return atoi(contents);

    return -1;
}

/* Make path to file in battery subdir */
static char const *_battery_path(int bat, char const *file)
{
    static char buf[BUFSIZ];
    snprintf(buf, sizeof(buf), "%sBAT%d/%s", POWER_SUPPLY_DIR, bat, file);
    return buf;
}

/* Return A/C status: 0 = offline, 1 = online */
int read_ac_status()
{
    return _read_int(POWER_SUPPLY_DIR "AC/online");
}

int read_battery(int bat, struct batt_info *info)
{
    char const *status;

    memset(info, 0, sizeof(*info));
    info->present = _read_int(_battery_path(bat, "present"));
    if (info->present < 0)
    {
	info->present = 0;
	return 0;
    }

    status = _read_str(_battery_path(bat, "status"));
    if (!status)
	return 0;

    if (!strcmp(status, "Charging"))
	info->status = BATTERY_CHARGING;
    else if (!strcmp(status, "Discharging"))
	info->status = BATTERY_DISCHARGING;
    else if (!strcmp(status, "Full"))
	info->status = BATTERY_FULL;

    info->energy_full = _read_int(_battery_path(bat, "energy_full"));
    info->energy_now  = _read_int(_battery_path(bat, "energy_now"));
    info->power_now   = _read_int(_battery_path(bat, "power_now"));
    info->voltage_now = _read_int(_battery_path(bat, "voltage_now"));
    if (info->status == BATTERY_DISCHARGING)
	info->power_now *= -1;

    if (info->voltage_now)
	info->current_now = round(1000.0 * info->power_now / info->voltage_now);

    if (info->power_now)
    {
	if (info->status == BATTERY_CHARGING)
	    info->time = round((3600.0 * (info->energy_full - info->energy_now))
		/ info->power_now);
	else
	    info->time = round(3600.0 * info->energy_now / abs(info->power_now));
    }

    return 1;
}
