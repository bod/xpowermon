#ifndef _power_h
#define _power_h

/*
 *  /sys/class/power_supply parsing for xpowermon.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

struct batt_info {
    /* these fields are set from /sys/class/power_supply/BATn */
    int present;	/* battery is online */
    enum {
	BATTERY_STATUS_UNKNOWN,
	BATTERY_CHARGING,
	BATTERY_DISCHARGING,
	BATTERY_FULL
    } status;		/* battery status (translated from string) */
    int energy_full;	/* total capacity (µWh) */
    int energy_now;	/* capacity (µWh) */
    int power_now;	/* power (µW) */
    int voltage_now;	/* voltage (µV) */

    /* calculated */
    int current_now;	/* current (mA) */
    int time;		/* time to discharge/charge (sec) */
};

int battery_count(void);
int read_ac_status(void);
int read_battery(int bat, struct batt_info *info);

#define POWER_SUPPLY_DIR "/sys/class/power_supply/"

#endif /* _power_h */
