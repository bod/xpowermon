#ifndef _version_h
#define _version_h

/*
 *  Define program version for C/shell.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#define VERSION	"2.0.0"

/* eval "$(cpp -DSET_SHELL_VAR=version version.h)" */
#ifdef SET_SHELL_VAR
SET_SHELL_VAR=VERSION
#endif

#endif /* _version_h */
