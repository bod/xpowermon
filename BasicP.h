#ifndef _BasicP_h
#define _BasicP_h

/*
 *  Basic widget private data.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include "Basic.h"
#include <X11/Xaw/SimpleP.h>

/* New fields for the Basic widget class record */
typedef struct {
    XtPointer extension;
} BasicClassPart;

/* Class record declaration */
typedef struct {
    CoreClassPart core_class;
    SimpleClassPart simple_class;
    BasicClassPart basic_class;
} BasicClassRec;

extern BasicClassRec basicClassRec;

/* New fields for the Basic widget record */
typedef struct {
    /* resources */
    Pixel foreground;		/* scale colour */
    String label;		/* field label */
    int value;			/* current value */
    String string_value;	/* or an alternate string */
    Dimension internal_width;	/* padding on either side of label */
    XFontStruct *font;		/* ...using font */
    String units;		/* ...and optional units */
    int scale_factor;		/* ...scaled by this factor */
    int precision;		/* ...decimal places */

    /* private state */
    GC _gc;			/* GC for basic */
} BasicPart;

/* Instance record declaration */
typedef struct _BasicRec {
    CorePart core;
    SimplePart simple;
    BasicPart basic;
} BasicRec;

#endif /* _BasicP_h */
