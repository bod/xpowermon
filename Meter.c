/*
 *  Meter widget implementation.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include <stdio.h>
#include <math.h>
#include <X11/StringDefs.h>
#include <X11/IntrinsicP.h>
#include "MeterP.h"
#include "misc.h"

/*
 * Class Methods
 */
static void meter_initialize(Widget request, Widget new, ArgList args,
    Cardinal *n_args);

static void meter_resize(Widget w);
static void meter_expose(Widget w, XEvent *event, Region region);
static Boolean meter_set_values(Widget current, Widget request, Widget new,
    ArgList args, Cardinal *n_args);

static void meter_destroy(Widget w);

#define	DEFAULT_PAD_X		4
#define	DEFAULT_PAD_Y		4
#define	DEFAULT_SCALE_X		100
#define	DEFAULT_SCALE_Y		50
#define DEFAULT_SCALE_FACTOR	1
#define DEFAULT_PRECISION	0

#define MAX_LABEL	64

#define DEG2RAD (3.14159265 / 180.0)

/*
 * Initialization
 */
#define offset(field) XtOffsetOf(MeterRec, field)
static XtResource resources[] = {
    {
	XtNheight, XtCHeight,
	XtRDimension, sizeof(Dimension),
	offset(core.height),
	XtRImmediate, (XtPointer) (DEFAULT_SCALE_Y + DEFAULT_PAD_Y * 2)
    },
    {
	XtNwidth, XtCWidth,
	XtRDimension, sizeof(Dimension),
	offset(core.width),
	XtRImmediate, (XtPointer) (DEFAULT_SCALE_X + DEFAULT_PAD_X * 2)
    },
    {
	XtNforeground, XtCForeground,
	XtRPixel, sizeof(Pixel),
	offset(meter.foreground),
	XtRString, XtDefaultForeground
    },
    {
	XtNneedleColor, XtCForeground,
	XtRPixel, sizeof(Pixel),
	offset(meter.needle),
	XtRString, XtDefaultForeground
    },
    {
	XtNminValue, XtCMinValue,
	XtRInt, sizeof(int),
	offset(meter.min),
	XtRImmediate, (XtPointer) 0
    },
    {
	XtNmaxValue, XtCMaxValue,
	XtRInt, sizeof(int),
	offset(meter.max),
	XtRImmediate, (XtPointer) 100
    },
    {
	XtNvalue, XtCValue,
	XtRInt, sizeof(int),
	offset(meter.value),
	XtRImmediate, (XtPointer) 0
    },
    {
	XtNinternalWidth, XtCWidth,
	XtRDimension, sizeof(Dimension),
	offset(meter.internal_width),
	XtRImmediate, (XtPointer) DEFAULT_PAD_X
    },
    {
	XtNinternalHeight, XtCHeight,
	XtRDimension, sizeof(Dimension),
	offset(meter.internal_height),
	XtRImmediate, (XtPointer) DEFAULT_PAD_Y
    },
    {
	XtNshowValue, XtCShowValue,
	XtRBoolean, sizeof(Boolean),
	offset(meter.show_value),
	XtRImmediate, (XtPointer) True
    },
    {
	XtNfont, XtCFont,
	XtRFontStruct, sizeof(XFontStruct*),
	offset(meter.font),
	XtRString, XtDefaultFont
    },
    {
	XtNfontColor, XtCFontColor,
	XtRPixel, sizeof(Pixel),
	offset(meter.font_color),
	XtRString, XtDefaultForeground
    },
    {
	XtNprecision, XtCPrecision,
	XtRInt, sizeof(int),
	offset(meter.precision),
	XtRImmediate, (XtPointer) DEFAULT_PRECISION
    },
    {
	XtNscale, XtCScale,
	XtRInt, sizeof(int),
	offset(meter.scale_factor),
	XtRImmediate, (XtPointer) DEFAULT_SCALE_FACTOR
    },
    {
	XtNunits, XtCUnits,
	XtRString, sizeof(String),
	offset(meter.units),
	XtRString, NULL
    },
};
#undef offset

MeterClassRec meterClassRec = {
  /* core */
  {
    (WidgetClass) &simpleClassRec,	/* superclass */
    "Meter",				/* class_name */
    sizeof(MeterRec),			/* widget_size */
    NULL,				/* class_initialize */
    NULL,				/* class_part_initialize */
    False,				/* class_inited */
    meter_initialize,			/* initialize */
    NULL,				/* initialize_hook */
    XtInheritRealize,			/* realize */
    NULL,				/* actions */
    0,					/* num_actions */
    resources,				/* resources */
    XtNumber(resources),		/* num_resources */
    NULLQUARK,				/* xrm_class */
    True,				/* compress_motion */
    True,				/* compress_exposure */
    True,				/* compress_enterleave */
    False,				/* visible_interest */
    meter_destroy,			/* destroy */
    meter_resize,			/* resize */
    meter_expose,			/* expose */
    meter_set_values,			/* set_values */
    NULL,				/* set_values_hook */
    XtInheritSetValuesAlmost,		/* set_values_almost */
    NULL,				/* get_values_hook */
    NULL,				/* accept_focus */
    XtVersion,				/* version */
    NULL,				/* callback_private */
    NULL,				/* tm_table */
    XtInheritQueryGeometry,		/* query_geometry */
    XtInheritDisplayAccelerator,	/* display_accelerator */
    NULL,				/* extension */
  },
  /* simple */
  {
    XtInheritChangeSensitive,		/* change_sensitive */
    NULL,
  },
  /* meter */
  {
    NULL,				/* extension */
  }
};

WidgetClass meterWidgetClass = (WidgetClass) &meterClassRec;

/*
 * Implementation
 */
#define XtWidth(w)	(w)->core.width
#define XtHeight(w)	(w)->core.height

static void meter_initialize(Widget request ATTRIB_UNUSED, Widget new,
    ArgList args ATTRIB_UNUSED, Cardinal *n_args ATTRIB_UNUSED)
{
    MeterWidget m = (MeterWidget) new;

    m->meter._scale = None;
    m->meter._scale_height = m->meter._scale_width = m->meter._scale_r = 0;
    m->meter._gc = None;
    m->meter._label_gc = None;
}

static void meter_destroy(Widget w)
{
    MeterWidget m = (MeterWidget) w;

    if (m->meter._scale)
	XFreePixmap(XtDisplay(w), m->meter._scale);

    if (m->meter._gc)
	XFreeGC(XtDisplay(w), m->meter._gc);

    if (m->meter._label_gc)
	XFreeGC(XtDisplay(w), m->meter._label_gc);
}

/*
 * The scale is the segment of a circle from -60� to +60�.  The
 * drawing area comprises of a rectangle which exactly contains this
 * arc, extended down by half the distance to the centre point.
 */
#define SCALE_RATIO 2.3094010767585
#define SIN60	    0.866025403784439
#define RANGE	    120

static void _redraw(MeterWidget m)
{
    int cx = (XtWidth(m) - m->meter._scale_width) / 2;
    int cy = (XtHeight(m) - m->meter._scale_height) / 2;
    int rx = cx + m->meter._scale_width / 2;
    int ry = cy + m->meter._scale_r;
    double deg;

    XClearWindow(XtDisplay(m), XtWindow(m));

    if (!m->meter._gc)
    {
	XGCValues gcv;

	gcv.foreground = m->meter.needle;
	gcv.background = m->core.background_pixel;
	gcv.line_width = 0;
	m->meter._gc = XCreateGC(XtDisplay(m), XtWindow(m),
	    GCForeground|GCBackground|GCLineWidth, &gcv);
    }

    XCopyArea(XtDisplay(m), m->meter._scale, XtWindow(m),
	m->meter._gc, 0, 0, m->meter._scale_width, m->meter._scale_height,
	cx, cy);

    if (m->meter.show_value)
    {
	char label[MAX_LABEL + 1];
	int label_sz = snprintf(label, MAX_LABEL, "%0.*f%s",
	    m->meter.precision,
	    ((double) m->meter.value / m->meter.scale_factor),
	    m->meter.units ? m->meter.units : "");

	int x;

	if (!m->meter._label_gc)
	{
	    XGCValues gcv;

	    gcv.foreground = m->meter.font_color;
	    gcv.background = m->core.background_pixel;
	    gcv.font = m->meter.font->fid;
	    m->meter._label_gc = XCreateGC(XtDisplay(m), XtWindow(m),
		GCForeground|GCBackground|GCFont, &gcv);
	}

	x = rx - XTextWidth(m->meter.font, label, label_sz) / 2;
	XDrawString(XtDisplay(m), XtWindow(m), m->meter._label_gc,
	    x, cy + m->meter._scale_height, label, label_sz);
    }

    if (m->meter.value < m->meter.min)
	deg = -1;
    else if (m->meter.value > m->meter.max)
	deg = RANGE + 1;
    else
    {
	deg = (double) (m->meter.value - m->meter.min) /
	    (m->meter.max - m->meter.min) * RANGE;
    }

    {
	double rad = (deg + 210) * DEG2RAD;
	double r = m->meter._scale_r * 1.1;

	XDrawLine(XtDisplay(m), XtWindow(m), m->meter._gc,
	    rx, ry, rx + round(r * cos(rad)), ry + round(r * sin(rad)));
    }
}

static void meter_resize(Widget w)
{
    MeterWidget m = (MeterWidget) w;
    int width = XtWidth(m) - 2 * m->meter.internal_width;
    int height = XtHeight(m) - 2 * m->meter.internal_height;

    if (m->meter._scale)
	XFreePixmap(XtDisplay(m), m->meter._scale);

    m->meter._scale = None;
    m->meter._scale_width = m->meter._scale_height = m->meter._scale_r = 0;

    if (!XtIsRealized(w) || width < 1 || height < 1)
	return;

    m->meter._scale_width = width;
    m->meter._scale_height = round(width / SCALE_RATIO);
    if (m->meter._scale_height > height)
    {
	m->meter._scale_height = height;
	m->meter._scale_width = round(height * SCALE_RATIO);
    }

    m->meter._scale_r = round((m->meter._scale_width / 2) / SIN60);
    m->meter._scale = XCreatePixmap(XtDisplay(m), XtWindow(m),
	m->meter._scale_width, m->meter._scale_height, m->core.depth);

    {
	int cx = m->meter._scale_width / 2;
	int cy = m->meter._scale_r;
	int deg;

	double r = m->meter._scale_r;

	GC gc;
	XGCValues gcv;

	gcv.foreground = gcv.background = m->core.background_pixel;
	gcv.line_width = 0;
	gc = XCreateGC(XtDisplay(m), m->meter._scale,
	    GCForeground|GCBackground|GCLineWidth, &gcv);

	XFillRectangle(XtDisplay(m), m->meter._scale, gc, 0, 0,
	    m->meter._scale_width, m->meter._scale_height);

	gcv.foreground = m->meter.foreground;
	XChangeGC(XtDisplay(m), gc, GCForeground, &gcv);

	for (deg = 0; deg <= 60; deg += 6)
	{
	    double rad = (deg + 270) * DEG2RAD;
	    double tsize = deg % 30 ? 0.96 : 0.90;

	    int x1 = round(r * cos(rad));
	    int y1 = round(r * sin(rad));
	    int x2 = round(r * cos(rad) * tsize);
	    int y2 = round(r * sin(rad) * tsize);

	    XDrawLine(XtDisplay(m), m->meter._scale, gc,
		cx + x1, cy + y1, cx + x2, cy + y2);

	    if (deg)
		XDrawLine(XtDisplay(m), m->meter._scale, gc,
		    cx - x1, cy + y1, cx - x2, cy + y2);
	}
    }

    _redraw(m);
}

static void meter_expose(Widget w, XEvent *event ATTRIB_UNUSED,
    Region region ATTRIB_UNUSED)
{
    if (!XtIsRealized(w))
	return;

    meter_resize(w);
}

static Boolean meter_set_values(Widget current, Widget request ATTRIB_UNUSED,
    Widget new, ArgList args ATTRIB_UNUSED, Cardinal *n_args ATTRIB_UNUSED)
{
    MeterWidget c = (MeterWidget) current;
    MeterWidget n = (MeterWidget) new;

    if (!XtIsRealized(current))
	return 0;

    if (n->meter.min		 != c->meter.min		||
	n->meter.max		 != c->meter.max		||
	n->meter.value		 != c->meter.value		||
	n->meter.show_value	 != c->meter.show_value		||
	n->meter.font		 != c->meter.font		||
	n->meter.font_color	 != c->meter.font_color		||
	n->meter.units		 != c->meter.units)
    {
	if (n->meter._scale)
	    _redraw(n);
	else
	    meter_resize(new);
    }

    return 0;
}
