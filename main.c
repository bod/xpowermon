/*
 *  X11 battery monitor for Linux laptops.
 *
 *  Copyright (c) 2002, 2006, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <X11/Xaw/Form.h>
#include "misc.h"
#include "Gauge.h"
#include "Meter.h"
#include "Basic.h"
#include "layout.h"
#include "power.h"
#include "version.h"

#define DEFAULT_LAYOUT		"power;time;charge"
#define DEFAULT_POLL_INTERVAL	5000
#define MAX_LABEL		128

#define COPYRIGHT \
    "Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>\n" \
    "This is free software, licensed under the terms of the GNU General Public\n" \
    "License.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n" \
    "PARTICULAR PURPOSE.\n"

#define HELP \
    "`%s' is an X11 battery monitor for Linux laptops.\n" \
    "\n" \
    "Usage: %s [-toolkitoption ...] [-option ...]\n" \
    "\n" \
    "Options:\n" \
    " -layout LAYOUT   use the widgets described by LAYOUT for the interface\n" \
    " -poll INTERVAL   poll /sys/class/power_supply every INTERVAL milliseconds\n" \
    " -help            print this help, then exit\n" \
    " -version         print version number, then exit\n" \
    "\n" \
    "The interface layout is defined by the -layout option, which defines the set of\n" \
    "values from the battery to be presented in rows delimited by `;', and columns by\n" \
    "`,'.\n" \
    "\n" \
    "Available values are:  `current', `voltage', `power', `charge' and `time'.\n" \
    "\n" \
    "By default each value is the sum of all batteries, although appending _N will\n" \
    "include only the value from battery N (`charge_0' for example).\n" \
    "\n" \
    "The widget used to present the value may be defined by adding `(WIDGET)' after\n" \
    "the name, where the available widget types are: `meter', `hgauge', `vgauge'\n" \
    "or `basic'.\n" \
    "\n" \
    "The default layout is `" DEFAULT_LAYOUT "'.\n"

/* define resources for both batteries, 0 and 1 */
#define RES(c, r, v) \
    "*" #c      "." #r ": " v, \
    "*" #c "_0" "." #r ": " v, \
    "*" #c "_1" "." #r ": " v

static String fallback_res[] = {  
    "*font:			lucidasans-10",
    "*fontColor:		grey50",
    "*Basic.foreground:		grey50",
    "*Meter.needleColor:	red",
    "*Gauge.foreground:		green",
    "*Gauge.warningColor:	yellow",
    "*Gauge.criticalColor:	red",
    RES(current, minValue,	"-3000"),
    RES(current, maxValue,	"3000"),
    RES(current, units,		"\\040mA"),
    RES(current, label,		"Current"),
    RES(voltage, minValue,	"0"),
    RES(voltage, maxValue,	"18000000"),
    RES(voltage, scale,		"1000000"),
    RES(voltage, precision,	"2"),
    RES(voltage, units,		"\\040V"),
    RES(voltage, label,		"Voltage"),
    RES(power,   minValue,	"-20000000"),
    RES(power,   maxValue,	"20000000"),
    RES(power,   scale,		"1000000"),
    RES(power,   precision,	"2"),
    RES(power,   units,		"\\040W"),
    RES(power,   label,		"Power"),
    RES(charge,  units,		"%"),
    RES(charge,  label,		"Charge"),
    RES(time,    label,		"Time left"),
    NULL
};

static void update_info(XtPointer, XtIntervalId *);
static void quit(Widget, XEvent *, String *, Cardinal *);
static XtActionsRec actions[] = {
    { "quit", quit },
};

static struct opts {
    Boolean help;	/* show usage info */
    Boolean version;	/* show version */
    String layout;	/* widget layout */
    int poll;		/* poll interval */
} opts;

#define offset(field) XtOffsetOf(struct opts, field)
static XtResource resources[] = {
    {
	"help", "Help",
	XtRBoolean, sizeof(Boolean),
	offset(help),
	XtRImmediate, (XtPointer) False
    },
    {
	"version", "Version",
	XtRBoolean, sizeof(Boolean),
	offset(version),
	XtRImmediate, (XtPointer) False
    },
    {
	"layout", "Layout",
	XtRString, sizeof(String),
	offset(layout),
	XtRString, DEFAULT_LAYOUT
    },
    {
	"poll", "Poll",
	XtRInt, sizeof(int),
	offset(poll),
	XtRImmediate, (XtPointer) DEFAULT_POLL_INTERVAL
    },
};
#undef offset

static XrmOptionDescRec options[] = {
    { "-layout",	"layout",	XrmoptionSepArg, NULL },
    { "-poll",		"poll",		XrmoptionSepArg, NULL },
    { "-help",		"help",		XrmoptionNoArg,	(XtPointer) "true" },
    { "-version",	"version",	XrmoptionNoArg,	(XtPointer) "true" },

    /* allow GNU-style options as well for these standard options */
    { "--help",		"help",		XrmoptionNoArg,	(XtPointer) "true" },
    { "--version",	"version",	XrmoptionNoArg,	(XtPointer) "true" },
};

static Atom wm_delete_window;
static int batteries;
static int ac = 0;
static struct doodads **doodads;

int main(int argc, char *argv[])
{
    XtAppContext app;
    Widget shell = XtVaOpenApplication(
	&app, "XPowermon", options, XtNumber(options), &argc, argv,
	fallback_res, applicationShellWidgetClass,
	NULL
    );

    int row;
    Widget layout;
    char const *self = strrchr(*argv, '/');
    if (self)
	self++;
    else
	self = *argv;

    if (argc > 1)
    {
	fprintf(stderr, HELP, self, self);
	return 2;
    }

    XtVaGetApplicationResources(
	shell, (XtPointer) &opts, resources, XtNumber(resources),
	NULL
    );

    if (opts.help)
    {
	printf(HELP, self, self);
	return 0;
    }

    if (opts.version)
    {
	printf("%s %s\n\n" COPYRIGHT, self, VERSION);
	return 0;
    }

    batteries = battery_count();
    doodads = parse_layout(opts.layout, batteries);

    /* create layout box */
    layout = XtVaCreateManagedWidget(
	"layout", formWidgetClass, shell,
	NULL
    );

    /* create widgets */
    for (row = 0; doodads[row]; row++)
    {
	int col;
	for (col = 0; doodads[row][col].name; col++)
	{
	    WidgetClass class = None;
	    Arg args[3]; /* fromHoriz, fromVert and orientation */
	    int n = 0;

	    if (row)
	    {
		XtSetArg(args[n], XtNfromVert, doodads[row - 1][0].w);
		n++;
	    }

	    if (col)
	    {
		XtSetArg(args[n], XtNfromHoriz, doodads[row][col - 1].w);
		n++;
	    }

	    switch (doodads[row][col].style)
	    {
	    case style_meter:
		class = meterWidgetClass;
		break;

	    case style_hgauge:
		class = gaugeWidgetClass;
		break;

	    case style_vgauge:
		class = gaugeWidgetClass;
		XtSetArg(args[n], XtNorientation, XtorientVertical);
		n++;
		break;

	    case style_basic:
		class = basicWidgetClass;
	    }

	    doodads[row][col].w = XtCreateManagedWidget(
		doodads[row][col].name, class, layout,
		args, n
	    );
	}
    }

    XtRealizeWidget(shell);

    /* setup timer to read /sys/class/power_supply */
    update_info((XtPointer) shell, NULL);

    /* handle WM close event */
    wm_delete_window = XInternAtom(XtDisplay(shell), "WM_DELETE_WINDOW", False);
    XSetWMProtocols(XtDisplay(shell), XtWindow(shell), &wm_delete_window, 1);
    XtAppAddActions(app, actions, XtNumber(actions));
    XtOverrideTranslations(shell,
	XtParseTranslationTable("<Message>WM_PROTOCOLS: quit()")
    );

    XtAppMainLoop(app);

    /*NOTREACHED*/
    return 0;
}

static void update_info(XtPointer data, XtIntervalId *id ATTRIB_UNUSED)
{
    Widget w = (Widget) data;
    struct batt_info info;
    int batt;
    int row;

    ac = read_ac_status();

    for (batt = 0; batt < batteries; batt++)
    {
	if (!read_battery(batt, &info))
	    continue;

	for (row = 0; doodads[row]; row++)
	{
	    int col;
	    for (col = 0; doodads[row][col].name; col++)
	    {
		/* initialise value before processing the first battery */
		if (!batt)
		    doodads[row][col].value = doodads[row][col].total = 0;

		/* no battery */
		if (!info.present)
		    continue;

		/* check if widget deals with this battery (or all) */
		if (doodads[row][col].battery != batt &&
		    doodads[row][col].battery != -1)
		    continue;

		switch (doodads[row][col].type)
		{
		case type_current:
		    doodads[row][col].value += info.current_now;
		    break;

		case type_voltage:
		    doodads[row][col].value += info.voltage_now;
		    break;

		case type_power:
		    doodads[row][col].value += info.power_now;
		    break;

		case type_charge:
		    doodads[row][col].value += info.energy_now;
		    doodads[row][col].total += info.energy_full;
		    break;

		case type_time:
		    /* add discharge time, or charge time if charging */
		    if (ac != 1 || info.status == BATTERY_CHARGING)
			doodads[row][col].value += info.time;
		}
	    }
	}
    }

    for (row = 0; doodads[row]; row++)
    {
	int col;
	for (col = 0; doodads[row][col].name; col++)
	{
	    switch (doodads[row][col].style)
	    {
	    case style_basic:
		if (doodads[row][col].type == type_time)
		{
		    int m = doodads[row][col].value / 60;
		    snprintf(doodads[row][col].string_value, SVALUE_SZ,
			"%d:%02d", m / 60, m % 60);

		    XtVaSetValues(
			doodads[row][col].w,
			XtNvalue, doodads[row][col].value,
			XtNstringValue, doodads[row][col].string_value,
			NULL
		    );

		    break;
		}

		/*FALLTHRU*/

	    case style_meter:
	    case style_hgauge:
	    case style_vgauge:
		{
		    int v;

		    if (doodads[row][col].total)
			v = round(doodads[row][col].value * 100.0 /
				  doodads[row][col].total);
		    else
			v = doodads[row][col].value;

		    XtVaSetValues(
			doodads[row][col].w,
			XtNvalue, v,
			NULL
		    );
		}
	    }
	}
    }

    XtAppAddTimeOut(XtWidgetToApplicationContext(w), opts.poll,
	update_info, data);
}

static void quit(Widget w, XEvent *event, String *params ATTRIB_UNUSED,
    Cardinal *num_params ATTRIB_UNUSED)
{
    if (event->type == ClientMessage && 
	event->xclient.data.l[0] == (long) wm_delete_window)
    {
	XCloseDisplay(XtDisplay(w));
	exit(0);
    }
}
