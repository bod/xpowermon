#ifndef _Meter_h
#define _Meter_h

/*
 *  Meter widget.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include <X11/Xaw/Simple.h>

/* Resources:

 Name		     Class		RepType		Default Value
 ----		     -----		-------		-------------
 background	     Background		Pixel		XtDefaultBackground
 border		     BorderColor	Pixel		XtDefaultForeground
 borderWidth	     BorderWidth	Dimension	1
 cursor		     Cursor		Cursor		None
 cursorName	     Cursor		String		NULL
 destroyCallback     Callback		Pointer		NULL
 displayList	     DisplayList	XawDisplayList*	NULL
 font		     Font		XFontStruct*	XtDefaultFont
 fontColor	     FontColor		Pixel		XtDefaultForeground
 foreground	     Foreground		Pixel		XtDefaultForeground
 height		     Height		Dimension	0
 insensitiveBorder   Insensitive	Pixmap		Gray
 internalHeight	     Height		Dimension	2
 internalWidth	     Width		Dimension	4
 mappedWhenManaged   MappedWhenManaged	Boolean		True
 maxValue	     MaxValue		int		100
 minValue	     MinValue		int		0
 needleColor	     Foreground		Pixel		XtDefaultForeground
 pointerColor	     Foreground		Pixel		XtDefaultForeground
 pointerColorBackground Background	Pixel		XtDefaultBackground
 precision	     Precision		int		0
 scale		     Scale		int		1
 sensitive	     Sensitive		Boolean		True
 showValue	     ShowValue		Boolean		True
 tip		     Tip		String		NULL
 units		     Units		String		NULL
 value		     Value		int		0
 width		     Width		Dimension	0
 x		     Position		Position	0
 y		     Position		Position	0

*/

#ifndef XtNfontColor
# define XtNfontColor	"fontColor"
#endif
#ifndef XtCFontColor
# define XtCFontColor	"FontColor"
#endif
#ifndef XtNmaxValue
# define XtNmaxValue	"maxValue"
#endif
#ifndef XtCMaxValue
# define XtCMaxValue	"MaxValue"
#endif
#ifndef XtNminValue
# define XtNminValue	"minValue"
#endif
#ifndef XtCMinValue
# define XtCMinValue	"MinValue"
#endif
#ifndef XtNneedleColor
# define XtNneedleColor	"needleColor"
#endif
#ifndef XtNprecision
# define XtNprecision	"precision"
#endif
#ifndef XtCPrecision
# define XtCPrecision	"Precision"
#endif
#ifndef XtNscale
# define XtNscale	"scale"
#endif
#ifndef XtCScale
# define XtCScale	"Scale"
#endif
#ifndef XtNshowValue
# define XtNshowValue	"showValue"
#endif
#ifndef XtCShowValue
# define XtCShowValue	"ShowValue"
#endif
#ifndef XtNunits
# define XtNunits	"units"
#endif
#ifndef XtCUnits
# define XtCUnits	"Units"
#endif

/* Class record constants */
extern WidgetClass meterWidgetClass;

typedef struct _MeterClassRec *MeterWidgetClass;
typedef struct _MeterRec      *MeterWidget;

#endif /* _Meter_h */
