/*
 *  Gauge widget implementation.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include <stdio.h>
#include <X11/StringDefs.h>
#include <X11/IntrinsicP.h>
#include "GaugeP.h"
#include "misc.h"

/*
 * Class Methods
 */
static void gauge_initialize(Widget request, Widget new, ArgList args,
    Cardinal *n_args);

static void gauge_resize(Widget w);
static void gauge_expose(Widget w, XEvent *event, Region region);
static Boolean gauge_set_values(Widget current, Widget request, Widget new,
    ArgList args, Cardinal *n_args);

static void gauge_destroy(Widget w);

#define	DEFAULT_H_WIDTH		108
#define	DEFAULT_H_HEIGHT	20
#define	DEFAULT_V_WIDTH		20
#define	DEFAULT_V_HEIGHT	58
#define DEFAULT_WARN_LEVEL	20
#define DEFAULT_CRIT_LEVEL	10
#define DEFAULT_SCALE_FACTOR	1
#define DEFAULT_PRECISION	0
#define MAX_LABEL		64

/*
 * Initialization
 */
#define offset(field) XtOffsetOf(GaugeRec, field)
static XtResource resources[] = {
    {
	XtNforeground, XtCForeground,
	XtRPixel, sizeof(Pixel),
	offset(gauge.foreground),
	XtRString, XtDefaultForeground
    },
    {
	XtNwarningColor, XtCForeground,
	XtRPixel, sizeof(Pixel),
	offset(gauge.warn),
	XtRString, XtDefaultForeground
    },
    {
	XtNcriticalColor, XtCForeground,
	XtRPixel, sizeof(Pixel),
	offset(gauge.crit),
	XtRString, XtDefaultForeground
    },
    {
	XtNminValue, XtCMinValue,
	XtRInt, sizeof(int),
	offset(gauge.min),
	XtRImmediate, (XtPointer) 0
    },
    {
	XtNmaxValue, XtCMaxValue,
	XtRInt, sizeof(int),
	offset(gauge.max),
	XtRImmediate, (XtPointer) 100
    },
    {
	XtNvalue, XtCValue,
	XtRInt, sizeof(int),
	offset(gauge.value),
	XtRImmediate, (XtPointer) 0
    },
    {
	XtNwarningLevel, XtCWarningLevel,
	XtRInt, sizeof(int),
	offset(gauge.warn_level),
	XtRImmediate, (XtPointer) DEFAULT_WARN_LEVEL
    },
    {
	XtNcriticalLevel, XtCCriticalLevel,
	XtRInt, sizeof(int),
	offset(gauge.crit_level),
	XtRImmediate, (XtPointer) DEFAULT_CRIT_LEVEL
    },
    {
	XtNshowValue, XtCShowValue,
	XtRBoolean, sizeof(Boolean),
	offset(gauge.show_value),
	XtRImmediate, (XtPointer) True
    },
    {
	XtNfont, XtCFont,
	XtRFontStruct, sizeof(XFontStruct*),
	offset(gauge.font),
	XtRString, XtDefaultFont
    },
    {
	XtNfontColor, XtCFontColor,
	XtRPixel, sizeof(Pixel),
	offset(gauge.font_color),
	XtRString, XtDefaultForeground
    },
    {
	XtNprecision, XtCPrecision,
	XtRInt, sizeof(int),
	offset(gauge.precision),
	XtRImmediate, (XtPointer) DEFAULT_PRECISION
    },
    {
	XtNscale, XtCScale,
	XtRInt, sizeof(int),
	offset(gauge.scale_factor),
	XtRImmediate, (XtPointer) DEFAULT_SCALE_FACTOR
    },
    {
	XtNunits, XtCUnits,
	XtRString, sizeof(String),
	offset(gauge.units),
	XtRString, NULL
    },
    {
	XtNorientation, XtCOrientation,
	XtROrientation, sizeof(XtOrientation),
	offset(gauge.orient),
	XtRImmediate, (XtPointer) XtorientHorizontal
    },
};
#undef offset

GaugeClassRec gaugeClassRec = {
  /* core */
  {
    (WidgetClass) &simpleClassRec,	/* superclass */
    "Gauge",				/* class_name */
    sizeof(GaugeRec),			/* widget_size */
    NULL,				/* class_initialize */
    NULL,				/* class_part_initialize */
    False,				/* class_inited */
    gauge_initialize,			/* initialize */
    NULL,				/* initialize_hook */
    XtInheritRealize,			/* realize */
    NULL,				/* actions */
    0,					/* num_actions */
    resources,				/* resources */
    XtNumber(resources),		/* num_resources */
    NULLQUARK,				/* xrm_class */
    True,				/* compress_motion */
    True,				/* compress_exposure */
    True,				/* compress_enterleave */
    False,				/* visible_interest */
    gauge_destroy,			/* destroy */
    gauge_resize,			/* resize */
    gauge_expose,			/* expose */
    gauge_set_values,			/* set_values */
    NULL,				/* set_values_hook */
    XtInheritSetValuesAlmost,		/* set_values_almost */
    NULL,				/* get_values_hook */
    NULL,				/* accept_focus */
    XtVersion,				/* version */
    NULL,				/* callback_private */
    NULL,				/* tm_table */
    XtInheritQueryGeometry,		/* query_geometry */
    XtInheritDisplayAccelerator,	/* display_accelerator */
    NULL,				/* extension */
  },
  /* simple */
  {
    XtInheritChangeSensitive,		/* change_sensitive */
    NULL,
  },
  /* gauge */
  {
    NULL,				/* extension */
  }
};

WidgetClass gaugeWidgetClass = (WidgetClass) &gaugeClassRec;

/*
 * Implementation
 */
#define XtWidth(w)	(w)->core.width
#define XtHeight(w)	(w)->core.height

static void gauge_initialize(Widget request ATTRIB_UNUSED, Widget new,
    ArgList args ATTRIB_UNUSED, Cardinal *n_args ATTRIB_UNUSED)
{
    GaugeWidget g = (GaugeWidget) new;

    if (XtWidth(g) < 1)
    {
	if (g->gauge.orient == XtorientHorizontal)
	    XtWidth(g) = DEFAULT_H_WIDTH;
	else
	    XtWidth(g) = DEFAULT_V_WIDTH;
    }

    if (XtHeight(g) < 1)
    {
	if (g->gauge.orient == XtorientHorizontal)
	    XtHeight(g) = DEFAULT_H_HEIGHT;
	else
	    XtHeight(g) = DEFAULT_V_HEIGHT;
    }

    g->gauge._gc = g->gauge._warn_gc = g->gauge._crit_gc =
	g->gauge._label_gc = None;
}

static void gauge_destroy(Widget w)
{
    GaugeWidget g = (GaugeWidget) w;

    if (g->gauge._gc)
	XFreeGC(XtDisplay(w), g->gauge._gc);

    if (g->gauge._warn_gc)
	XFreeGC(XtDisplay(w), g->gauge._warn_gc);

    if (g->gauge._crit_gc)
	XFreeGC(XtDisplay(w), g->gauge._crit_gc);

    if (g->gauge._label_gc)
	XFreeGC(XtDisplay(w), g->gauge._label_gc);
}

static void gauge_resize(Widget w)
{
    GaugeWidget g = (GaugeWidget) w;
    double percent;
    Pixel colour = g->gauge.foreground;
    GC *gc = &g->gauge._gc;

    if (!XtIsRealized(w) || XtWidth(w) < 1 || XtHeight(w) < 1)
	return;

    if (g->gauge.value < g->gauge.min)
	percent = 0;
    else if (g->gauge.value > g->gauge.max)
	percent = 100;
    else
    {
	percent = (double) (g->gauge.value - g->gauge.min) /
	    (g->gauge.max - g->gauge.min) * 100;
    }

    if (percent <= g->gauge.crit_level)
	colour = g->gauge.crit;
    else if (percent <= g->gauge.warn_level)
	colour = g->gauge.warn;

    if (colour == g->gauge.crit)
	gc = &g->gauge._crit_gc;
    else if (colour == g->gauge.warn)
	gc = &g->gauge._warn_gc;

    if (!*gc)
    {
	XGCValues gcv;

	gcv.foreground = colour;
	gcv.background = g->core.background_pixel;
	*gc = XCreateGC(XtDisplay(g), XtWindow(g),
	    GCForeground|GCBackground, &gcv);
    }

    XClearWindow(XtDisplay(g), XtWindow(g));
    {
	int x1 = 0;
	int y1;
	int x2;
	int y2 = XtHeight(g);

	if (g->gauge.orient == XtorientVertical)
	{
	    y1 = XtHeight(g) - XtHeight(g) * percent / 100;
	    x2 = XtWidth(g);
	}
	else
	{
	    y1 = 0;
	    x2 = XtWidth(g) * percent / 100;
	}

	XFillRectangle(XtDisplay(g), XtWindow(g), *gc, x1, y1, x2, y2);
    }

    if (g->gauge.show_value)
    {
	char label[MAX_LABEL + 1];
	int label_sz = snprintf(label, MAX_LABEL, "%0.*f%s",
	    g->gauge.precision,
	    ((double) g->gauge.value / g->gauge.scale_factor),
	    g->gauge.units ? g->gauge.units : "");

	int dir;
	int ascent;
	int descent;
	XCharStruct extents;
	int x;
	int y;
	int height;

	if (!g->gauge._label_gc)
	{
	    XGCValues gcv;

	    gcv.foreground = g->gauge.font_color;
	    gcv.background = g->core.background_pixel;
	    gcv.font = g->gauge.font->fid;
	    g->gauge._label_gc = XCreateGC(XtDisplay(g), XtWindow(g),
		GCForeground|GCBackground|GCFont, &gcv);
	}

	XTextExtents(g->gauge.font, label, label_sz, &dir, &ascent,
	    &descent, &extents);

	if (extents.width < XtWidth(g))
	    x = (XtWidth(g) - extents.width) / 2;
	else
	    x = 0;

	if ((height = extents.ascent + extents.descent) < XtWidth(g))
	    y = (XtHeight(g) - height) / 2 + extents.ascent;
	else
	    y = XtHeight(g);

	XDrawString(XtDisplay(g), XtWindow(g), g->gauge._label_gc, x, y,
	    label, label_sz);
    }
}

static void gauge_expose(Widget w, XEvent *event ATTRIB_UNUSED,
    Region region ATTRIB_UNUSED)
{
    if (!XtIsRealized(w))
	return;

    gauge_resize(w);
}

static Boolean gauge_set_values(Widget current, Widget request ATTRIB_UNUSED,
    Widget new, ArgList args ATTRIB_UNUSED, Cardinal *n_args ATTRIB_UNUSED)
{
    GaugeWidget c = (GaugeWidget) current;
    GaugeWidget n = (GaugeWidget) new;

    if (!XtIsRealized(current))
	return 0;

    if (n->gauge.min		!= c->gauge.min		||
	n->gauge.max		!= c->gauge.max		||
	n->gauge.value		!= c->gauge.value	||
	n->gauge.warn_level	!= c->gauge.warn_level	||
	n->gauge.crit_level	!= c->gauge.crit_level	||
	n->gauge.show_value	!= c->gauge.show_value	||
	n->gauge.font		!= c->gauge.font	||
	n->gauge.font_color	!= c->gauge.font_color	||
	n->gauge.units		!= c->gauge.units)
    {
	gauge_resize(new);
    }

    return 0;
}
