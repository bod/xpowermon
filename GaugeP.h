#ifndef _GaugeP_h
#define _GaugeP_h

/*
 *  Gauge widget private data.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include "Gauge.h"
#include <X11/Xaw/SimpleP.h>

/* New fields for the Gauge widget class record */
typedef struct {
    XtPointer extension;
} GaugeClassPart;

/* Class record declaration */
typedef struct {
    CoreClassPart core_class;
    SimpleClassPart simple_class;
    GaugeClassPart gauge_class;
} GaugeClassRec;

extern GaugeClassRec gaugeClassRec;

/* New fields for the Gauge widget record */
typedef struct {
    /* resources */
    Pixel foreground;		/* scale colour */
    Pixel warn;			/* warning colour */
    Pixel crit;			/* critical colour */
    int min;			/* leftmost value */
    int max;			/* rightmost value */
    int value;			/* current value */
    int warn_level;		/* use warn colour to this percent */
    int crit_level;		/* use crit colour to this percent */
    Boolean show_value;		/* display current value: */
    XFontStruct *font;		/* ...using font */
    Pixel font_color;		/* ...in colour */
    String units;		/* ...and optional units */
    int scale_factor;		/* ...scaled by this factor */
    int precision;		/* ...decimal places */
    XtOrientation orient;	/* horizontal or vertical */

    /* private state */
    GC _gc;			/* GC for gauge */
    GC _warn_gc;		/* GC for gauge's warn colour */
    GC _crit_gc;		/* GC for gauge's crit colour */
    GC _label_gc;		/* GC for label */
} GaugePart;

/* Instance record declaration */
typedef struct _GaugeRec {
    CorePart core;
    SimplePart simple;
    GaugePart gauge;
} GaugeRec;

#endif /* _GaugeP_h */
