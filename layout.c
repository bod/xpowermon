/*
 *  Layout resource parsing for xpowermon.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include <X11/Intrinsic.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <err.h>
#include "layout.h"

static struct types {
    char const *name;
    enum type type;
    enum style default_style;
} types[] = {
    { "current", type_current, style_meter },
    { "voltage", type_voltage, style_meter },
    { "power",   type_power,   style_meter },
    { "charge",  type_charge,  style_hgauge },
    { "time",    type_time,    style_basic },
    { NULL },
};

static struct styles {
    char const *name;
    enum style style;
} styles[] = {
    { "meter",  style_meter },
    { "hgauge", style_hgauge },
    { "vgauge", style_vgauge },
    { "basic",  style_basic },
    { NULL },
};

static struct types *lookup_type(char const *str)
{
    struct types *t = types;

    while (t->name)
	if (!strcmp(t->name, str))
	    return t;
	else
	    t++;

    return NULL;
}

static struct styles *lookup_style(char const *str)
{
    struct styles *s = styles;

    while (s->name)
	if (!strcmp(s->name, str))
	    return s;
	else
	    s++;

    return NULL;
}

static char *strip(char *s)
{
    while (isspace(*s))
	s++;

    {
	char *p = s + strlen(s) - 1;
	while (p >= s && isspace(*p))
	    *p-- = '\0';
    }

    return s;
}

struct doodads **parse_layout(char const *layout, int batteries)
{
    struct doodads **ret = NULL;
    int row = 0;

    if (!*layout)
	errx(2, "layout required");

    /* calculate the number of rows */
    {
	char const *p = layout;
	while (p)
	{
	    row++;
	    if ((p = strpbrk(p, ";\n")))
		p++;
	}
    }

    ret = (struct doodads **) XtMalloc((row + 1) * sizeof(struct doodads *));
    ret[row] = NULL;

    {
	char *tmp = XtNewString(layout);
	char *next_row = NULL;
	row = 0;

	do {
	    int col = 0;
	    char *next_col = NULL;

	    next_row = strpbrk(tmp, ";\n");
	    if (next_row)
		*next_row++ = '\0';

	    /* calculate the number of cols */
	    {
		char const *p = tmp;
		while (p)
		{
		    col++;
		    if ((p = strchr(p, ',')))
			p++;
		}
	    }

	    ret[row] = (struct doodads *)
		XtMalloc((col + 1) * sizeof(struct doodads));

	    ret[row][col].name = NULL;
	    col = 0;

	    do {
		int battery = -1;
		int style = -1;
		struct types *t;
		char *p;
		int len;

		next_col = strchr(tmp, ',');
		if (next_col)
		    *next_col++ = '\0';

		tmp = strip(tmp);
		len = strlen(tmp);
		if (tmp[len - 1] == ')' && (p = strchr(tmp, '(')))
		{
		    struct styles *sp = NULL;

		    tmp[len - 1] = *p++ = '\0';
		    p = strip(p);
		    if (!(sp = lookup_style(p)))
			errx(2, "invalid style specification `%s' (%d,%d)",
			    p, col, row);

		    style = sp->style;
		    tmp = strip(tmp);
		}

		if ((p = strchr(tmp, '_')))
		{
		    char *e;

		    *p++ = '\0';
		    battery = strtol(p, &e, 10);
		    if (*e || battery < 0 || battery >= batteries)
			errx(2, "invalid battery specification `%s' "
			    "(%d,%d)", p, col, row);
		}

		if (!(t = lookup_type(tmp)))
		    errx(2, "invalid type specification `%s' (%d,%d)",
			tmp, col, row);

		ret[row][col].name = t->name;
		ret[row][col].w = None;
		ret[row][col].style = style < 0 ? t->default_style
		  : (enum style) style;
		ret[row][col].type = t->type;
		ret[row][col].battery = battery;
		ret[row][col].value = 0;
		ret[row][col].total = 0;
		ret[row][col].string_value[0] = '\0';

		if (battery != -1)
		{
		    char *name;
		    /* re-append the battery number */
		    sprintf(tmp + strlen(tmp), "_%d", battery);
		    name = XtMalloc(strlen(tmp) + 1);
		    ret[row][col].name = strcpy(name, tmp);
		}

		col++;
	    } while ((tmp = next_col));

	    row++;
	} while ((tmp = next_row));

	XtFree(tmp);
    }

    return ret;
}
