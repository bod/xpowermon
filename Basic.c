/*
 *  Basic widget implementation.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include <stdio.h>
#include <string.h>
#include <X11/StringDefs.h>
#include <X11/IntrinsicP.h>
#include "BasicP.h"
#include "misc.h"

/*
 * Class Methods
 */
static void basic_initialize(Widget request, Widget new, ArgList args,
    Cardinal *n_args);

static void basic_resize(Widget w);
static void basic_expose(Widget w, XEvent *event, Region region);
static Boolean basic_set_values(Widget current, Widget request, Widget new,
    ArgList args, Cardinal *n_args);

static void basic_destroy(Widget w);

#define	DEFAULT_WIDTH		108
#define	DEFAULT_HEIGHT		20
#define	DEFAULT_PAD_X		4
#define DEFAULT_SCALE_FACTOR	1
#define DEFAULT_PRECISION	0
#define MAX_LABEL		128

/*
 * Initialization
 */
#define offset(field) XtOffsetOf(BasicRec, field)
static XtResource resources[] = {
    {
	XtNheight, XtCHeight,
	XtRDimension, sizeof(Dimension),
	offset(core.height),
	XtRImmediate, (XtPointer) DEFAULT_HEIGHT
    },
    {
	XtNwidth, XtCWidth,
	XtRDimension, sizeof(Dimension),
	offset(core.width),
	XtRImmediate, (XtPointer) DEFAULT_WIDTH
    },
    {
	XtNinternalWidth, XtCWidth,
	XtRDimension, sizeof(Dimension),
	offset(basic.internal_width),
	XtRImmediate, (XtPointer) DEFAULT_PAD_X
    },
    {
	XtNforeground, XtCForeground,
	XtRPixel, sizeof(Pixel),
	offset(basic.foreground),
	XtRString, XtDefaultForeground
    },
    {
	XtNlabel, XtCLabel,
	XtRString, sizeof(String),
	offset(basic.label),
	XtRString, NULL
    },
    {
	XtNvalue, XtCValue,
	XtRInt, sizeof(int),
	offset(basic.value),
	XtRImmediate, (XtPointer) 0
    },
    {
	XtNstringValue, XtCStringValue,
	XtRString, sizeof(String),
	offset(basic.string_value),
	XtRString, NULL
    },
    {
	XtNfont, XtCFont,
	XtRFontStruct, sizeof(XFontStruct*),
	offset(basic.font),
	XtRString, XtDefaultFont
    },
    {
	XtNprecision, XtCPrecision,
	XtRInt, sizeof(int),
	offset(basic.precision),
	XtRImmediate, (XtPointer) DEFAULT_PRECISION
    },
    {
	XtNscale, XtCScale,
	XtRInt, sizeof(int),
	offset(basic.scale_factor),
	XtRImmediate, (XtPointer) DEFAULT_SCALE_FACTOR
    },
    {
	XtNunits, XtCUnits,
	XtRString, sizeof(String),
	offset(basic.units),
	XtRString, NULL
    },
};
#undef offset

BasicClassRec basicClassRec = {
  /* core */
  {
    (WidgetClass) &simpleClassRec,	/* superclass */
    "Basic",				/* class_name */
    sizeof(BasicRec),			/* widget_size */
    NULL,				/* class_initialize */
    NULL,				/* class_part_initialize */
    False,				/* class_inited */
    basic_initialize,			/* initialize */
    NULL,				/* initialize_hook */
    XtInheritRealize,			/* realize */
    NULL,				/* actions */
    0,					/* num_actions */
    resources,				/* resources */
    XtNumber(resources),		/* num_resources */
    NULLQUARK,				/* xrm_class */
    True,				/* compress_motion */
    True,				/* compress_exposure */
    True,				/* compress_enterleave */
    False,				/* visible_interest */
    basic_destroy,			/* destroy */
    basic_resize,			/* resize */
    basic_expose,			/* expose */
    basic_set_values,			/* set_values */
    NULL,				/* set_values_hook */
    XtInheritSetValuesAlmost,		/* set_values_almost */
    NULL,				/* get_values_hook */
    NULL,				/* accept_focus */
    XtVersion,				/* version */
    NULL,				/* callback_private */
    NULL,				/* tm_table */
    XtInheritQueryGeometry,		/* query_geometry */
    XtInheritDisplayAccelerator,	/* display_accelerator */
    NULL,				/* extension */
  },
  /* simple */
  {
    XtInheritChangeSensitive,		/* change_sensitive */
    NULL,
  },
  /* basic */
  {
    NULL,				/* extension */
  }
};

WidgetClass basicWidgetClass = (WidgetClass) &basicClassRec;

/*
 * Implementation
 */
#define XtWidth(w)	(w)->core.width
#define XtHeight(w)	(w)->core.height

static void basic_initialize(Widget request ATTRIB_UNUSED, Widget new,
    ArgList args ATTRIB_UNUSED, Cardinal *n_args ATTRIB_UNUSED)
{
    BasicWidget b = (BasicWidget) new;

    b->basic._gc = None;
}

static void basic_destroy(Widget w)
{
    BasicWidget b = (BasicWidget) w;

    if (b->basic._gc)
	XFreeGC(XtDisplay(w), b->basic._gc);
}

static void basic_resize(Widget w)
{
    BasicWidget b = (BasicWidget) w;
    char str[MAX_LABEL + 1];
    int label_sz = 0;
    int value_sz;

    if (!XtIsRealized(w) || XtWidth(w) < 1 || XtHeight(w) < 1)
	return;

    if (!b->basic._gc)
    {
	XGCValues gcv;

	gcv.foreground = b->basic.foreground;
	gcv.background = b->core.background_pixel;
	gcv.font = b->basic.font->fid;
	b->basic._gc = XCreateGC(XtDisplay(b), XtWindow(b),
	    GCForeground|GCBackground|GCFont, &gcv);
    }

    XClearWindow(XtDisplay(b), XtWindow(b));

    if (b->basic.label)
	label_sz = snprintf(str, MAX_LABEL, "%s:", b->basic.label);

    if (b->basic.string_value)
	value_sz = snprintf(str + label_sz, MAX_LABEL - label_sz,
	    "%s", b->basic.string_value);
    else
	value_sz = snprintf(str + label_sz, MAX_LABEL - label_sz,
	    "%0.*f%s", b->basic.precision,
	    ((double) b->basic.value / b->basic.scale_factor),
	    b->basic.units ? b->basic.units : "");

    {
	int dir;
	int ascent;
	int descent;
	XCharStruct extents;
	int x;
	int y;
	int height;

	XTextExtents(b->basic.font, str, label_sz + value_sz, &dir, &ascent,
	    &descent, &extents);

	if ((height = extents.ascent + extents.descent) < XtWidth(b))
	    y = (XtHeight(b) - height) / 2 + extents.ascent;
	else
	    y = XtHeight(b);

	x = XtWidth(b) - b->basic.internal_width * 2 - extents.width;
	if (x > 0)
	{
	    XDrawString(XtDisplay(b), XtWindow(b), b->basic._gc,
		b->basic.internal_width, y, str, label_sz);

	    x += b->basic.internal_width +
		XTextWidth(b->basic.font, str, label_sz);

	    XDrawString(XtDisplay(b), XtWindow(b), b->basic._gc, x, y,
		str + label_sz, value_sz);
	}
	else
	    XDrawString(XtDisplay(b), XtWindow(b), b->basic._gc, 0, y,
		str, label_sz + value_sz);
    }
}

static void basic_expose(Widget w, XEvent *event ATTRIB_UNUSED,
    Region region ATTRIB_UNUSED)
{
    if (!XtIsRealized(w))
	return;

    basic_resize(w);
}

static Boolean basic_set_values(Widget current, Widget request ATTRIB_UNUSED,
    Widget new, ArgList args ATTRIB_UNUSED, Cardinal *n_args ATTRIB_UNUSED)
{
    BasicWidget c = (BasicWidget) current;
    BasicWidget n = (BasicWidget) new;

    if (!XtIsRealized(current))
	return 0;

    if (n->basic.value		!= c->basic.value		||
	n->basic.font		!= c->basic.font		||
	n->basic.units		!= c->basic.units)
    {
	basic_resize(request);
    }

    return 0;
}
