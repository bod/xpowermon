#ifndef _layout_h
#define _layout_h

/*
 *  Layout resource parsing for xpowermon.
 *
 *  Copyright (c) 2002, 2013  Brendan O'Dea <bod@debian.org>
 *
 *  This program is free software:  you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

enum type  { type_current, type_voltage, type_power, type_charge, type_time };
enum style { style_meter, style_hgauge, style_vgauge, style_basic };

struct doodads {
    char const *name;	/* widget name */
    Widget w;		/* widget instance */
    enum style style;	/* widget style (meter, hgauge, ...) */
    enum type type;	/* widget type (current, voltage, ...) */
    int battery;	/* battery number (from 0, -1 = sum of all batteries) */
    int value;		/* widget value */
    int total;		/* max widget value (for charge %) */
#define SVALUE_SZ 6
    char string_value[SVALUE_SZ + 1];
			/* for "time" style, up to nnn:nn */
};

struct doodads **parse_layout(char const *layout, int batteries);

#endif /* _layout_h */
